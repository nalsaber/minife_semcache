#ifndef miniFE_info_hpp
#define miniFE_info_hpp

#define MINIFE_HOSTNAME "io.ecn.purdue.edu"
#define MINIFE_KERNEL_NAME "'Linux'"
#define MINIFE_KERNEL_RELEASE "'2.6.32-504.el6.x86_64'"
#define MINIFE_PROCESSOR "'x86_64'"

#define MINIFE_CXX "'/var/scratch/nalsaber/cuda6/bin/nvcc'"
#define MINIFE_CXX_VERSION "'nvcc: NVIDIA (R) Cuda compiler driver'"
#define MINIFE_CXXFLAGS "''"

#endif
