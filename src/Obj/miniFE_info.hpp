#ifndef miniFE_info_hpp
#define miniFE_info_hpp

#define MINIFE_HOSTNAME "io.ecn.purdue.edu"
#define MINIFE_KERNEL_NAME "'Linux'"
#define MINIFE_KERNEL_RELEASE "'2.6.32-504.el6.x86_64'"
#define MINIFE_PROCESSOR "'x86_64'"

#define MINIFE_CXX "'/opt/mpich2-gcc/1.4.1p1/bin/mpicxx'"
#define MINIFE_CXX_VERSION "'c++ (GCC) 4.4.7 20120313 (Red Hat 4.4.7-11)'"
#define MINIFE_CXXFLAGS "''"

#endif
