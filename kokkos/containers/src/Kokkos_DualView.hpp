/*
//@HEADER
// ************************************************************************
//
//                             Kokkos
//         Manycore Performance-Portable Multidimensional Arrays
//
//              Copyright (2012) Sandia Corporation
//
// Under the terms of Contract DE-AC04-94AL85000 with Sandia Corporation,
// the U.S. Government retains certain rights in this software.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.
//
// 3. Neither the name of the Corporation nor the names of the
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY SANDIA CORPORATION "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL SANDIA CORPORATION OR THE
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Questions?  Contact  H. Carter Edwards (hcedwar@sandia.gov)
//
// ************************************************************************
//@HEADER
*/


/* DualView: container class to manage data structures which exist both on Host and Device
 * member functions:
 * DualView()
 * DualView(label,dim0,dim1,dim2,...)
 * view<DeviceType>()
 * sync<DeviceType>()
 * modify<DeviceType>()
 * resize(dim0,dim1,dim2,...)
 */
#ifndef KOKKOS_DUALVIEW_HPP
#define KOKKOS_DUALVIEW_HPP

#include <list>
#include <signal.h>
#include <errno.h>
#include <sys/mman.h>
#include <ucontext.h> 
#define PageSize (1LLU << 12LLU)
#define PageMask (PageSize - 1LLU)
#define PageCeiling(objSize) ((objSize + PageSize - 1) & ~PageMask)

#include <Kokkos_View.hpp>
namespace Kokkos {
	
struct TranslationRecord{
  void* HostStartAddress;
  void* HostEndAddress;
  void* DeviceStartAddress;
  int Size;
  int Size_Padded;
  int DataType;
  char Status;
  };
std::list<TranslationRecord*> cacheDirList;
	
class SemCache{
public:
    //std::ostringstream debugmsg ;

	TranslationRecord mainEntry;

	void SemCacheSet(void* _HostStartAddress, void* _HostEndAddress, void* _DeviceStartAddress, int _Size, int _DataType){
		mainEntry.HostStartAddress = _HostStartAddress;
		mainEntry.HostEndAddress = _HostEndAddress;
		mainEntry.DeviceStartAddress = _DeviceStartAddress;
		mainEntry.Size = _Size;
		mainEntry.DataType = _DataType;
		mainEntry.Size_Padded = PageCeiling(_DataType * _Size);//page aligned padding
	}

	static void invalidateOnGPU(TranslationRecord* entry) {
		(*entry).Status = 'C';
	}
	static void invalidateOnCPU(TranslationRecord* entry) {
		(*entry).Status = 'G';
	}

	//called after invoking a GPU method that writes [s, e)
	void writeGPU() {
		if(mainEntry.HostStartAddress == 0)
			return;

		TranslationRecord* entry = lookupAndAdd();
		invalidateOnCPU(entry);
		if( mprotect(mainEntry.HostStartAddress , mainEntry.Size_Padded ,  PROT_NONE) != 0){
			std::cout << "mprotect none failed addr " << mainEntry.HostStartAddress << "\n";
		}
	}

	//called before invoking a GPU method that reads [s, e)
	 void readGPU() {
		if(mainEntry.HostStartAddress == 0)
			return;

		TranslationRecord* entry = lookupAndAdd();
		if ((*entry).Status == 'C'){ //data not current on GPU
			Kokkos::Impl::DeepCopy<CudaSpace,HostSpace>::DeepCopy(mainEntry.DeviceStartAddress, mainEntry.HostStartAddress,  mainEntry.Size * mainEntry.DataType ); //transferToGPU

			//std::cout << "mprotect range " << &h_view(0) << "-" << &h_view(0)+(*entry).Size_Padded  << " size:" << (*entry).Size_Padded * (*entry).DataType << "\n";
			if( mprotect(mainEntry.HostStartAddress , mainEntry.Size_Padded,  PROT_READ) != 0){
				std::cout << "mprotect failed addr " << mainEntry.HostStartAddress << "\n";
			}
			(*entry).Status = 'S';
		}
	}

	 //execute after writing address range [s, e) on CPU
	 static void  writeCPU(TranslationRecord* entry) {
		 //TranslationRecord* entry = lookupAndAdd();
		 if ((*entry).Status == 'G'){ //data not current on CPU
			 Kokkos::Impl::DeepCopy<HostSpace,CudaSpace>::DeepCopy(entry->HostStartAddress, entry->DeviceStartAddress, entry->Size * entry->DataType );
		 }
		 invalidateOnGPU(entry);
	 }

	 //execute before reading address range [s, e) on CPU
	 static void readCPU(TranslationRecord* entry) {
		 //TranslationRecord* entry = lookupAndAdd();
		 if ((*entry).Status == 'G'){ //data not current on CPU
			 Kokkos::Impl::DeepCopy<HostSpace,CudaSpace>::DeepCopy(entry->HostStartAddress, entry->DeviceStartAddress, entry->Size * entry->DataType );

			 (*entry).Status = 'S';
		 }
		 if(mprotect(entry->HostStartAddress , entry->Size_Padded ,  PROT_READ) != 0){
			 std::cout << "**mprotect free failed: " << "\n";
		 }
	 }

	TranslationRecord* lookupAndAdd(){
		 for (std::list<TranslationRecord*>::iterator it=cacheDirList.begin(); it != cacheDirList.end(); ++it){
			 if( (*it)->HostStartAddress ==  mainEntry.HostStartAddress){// && (cacheDir[x].HostEndAddress >= HostEndAddress) ){
				//std::cout << "=list start addr " << (*it)->HostStartAddress  << "\n";
				 TranslationRecord* tr = *it;
				 return tr;
			 }
		 }
		 
		//std::cout << "....Add new address: " << mainEntry.HostStartAddress << " end addr:" << mainEntry.HostEndAddress  << " DeviceStartAddress: " << mainEntry.DeviceStartAddress << " size: " << mainEntry.Size << " Size_Padded: " << mainEntry.Size_Padded << " -size bytes " << mainEntry.Size * mainEntry.DataType << " unit size:" << mainEntry.DataType << "\n";

		mainEntry.Status = 'C';
		cacheDirList.push_back(&mainEntry);
		return &mainEntry;
	}

	static TranslationRecord existsInCacheDirRange(void* CPUptr)
	{
		for (std::list<TranslationRecord*>::iterator it=cacheDirList.begin(); it != cacheDirList.end(); ++it){
			if( ((*it)->HostStartAddress <=  CPUptr) && ((*it)->HostEndAddress >= CPUptr) ){
				TranslationRecord* tr = *it;
				return *tr;
			}
		}

		std::cout<< "SIG FAULT Address not found exiting \n"; 
		exit(EXIT_FAILURE);
	}

	static void remove(void* CPUptr){
		for (std::list<TranslationRecord*>::iterator it=cacheDirList.begin(); it != cacheDirList.end(); ++it){
			if( (*it)->HostStartAddress ==  CPUptr){
				std::cout << "= remove list start addr " << (*it)->HostStartAddress  << "\n";
				cacheDirList.erase(it);
			}
		}
	}
	static void handler(int sig, siginfo_t *si, void *uap)
	{
		ucontext_t *context =  (ucontext_t *) uap;
		int write_fault = context->uc_mcontext.gregs[REG_ERR];
		//std::cout<< "Got SIGSEGV at address: "<<  si->si_addr << " REG_ERR:" << write_fault << "\n"; 

		TranslationRecord translationRecord = existsInCacheDirRange(si->si_addr);


		//std::cout << "Found - Address start: " << translationRecord.HostStartAddress << " sig addr: " << si->si_addr << 
		//	" end:" << translationRecord.HostEndAddress << 
		//	" free size:" << translationRecord.Size_Padded  << "\n"; 
		//" Size_Padded:" <<  cacheDir[x].Size_Padded << " sizeof:" << cacheDir[x].DataType <<
		//" free end address:" <<cacheDir[x].HostStartAddress +(cacheDir[x].Size_Padded  *cacheDir[x].DataType) << "\n";

		if(mprotect(translationRecord.HostStartAddress , translationRecord.Size_Padded,  PROT_READ|PROT_WRITE) != 0){
			std::cout << "**mprotect free failed: " << "\n";
			exit(EXIT_FAILURE);
		}

		if (write_fault == 4) //None -> Needs Read Access
		{
			readCPU(&translationRecord);
		}
		else if (write_fault == 6) //None -> Needs Write Access
		{
			writeCPU(&translationRecord);
		}
		else if (write_fault == 7) //ReadOnly -> Needs Write Access
		{
			invalidateOnGPU(&translationRecord);//Status = 'C';
		}
	}

	static void Init() //InitHandler
	{
		int pagesize = sysconf(_SC_PAGE_SIZE);
		struct sigaction sa;
		sa.sa_flags = SA_SIGINFO;
		sigemptyset(&sa.sa_mask);
		sa.sa_sigaction = handler;
		if (sigaction(SIGSEGV, &sa, NULL) == -1){
			perror("sigaction"); 
			exit(EXIT_FAILURE);
		}
	}

};

template< class T , class L , class D>
class DualView : public SemCache{
public:

  /* Define base types for Device and Host */

  typedef Kokkos::View<T,L,D> t_dev ;
  typedef typename t_dev::HostMirror t_host ;

  /* Define typedefs for different usage scenarios */

  // Define const view types
  typedef Kokkos::View<typename t_dev::const_data_type,L,D> t_dev_const;
  typedef typename t_dev_const::HostMirror t_host_const;

  // Define const randomread view types
  typedef Kokkos::View<typename t_dev::const_data_type,L,D,Kokkos::MemoryRandomRead> t_dev_const_randomread ;
  typedef typename t_dev_const_randomread::HostMirror t_host_const_randomread;

  // Define unmanaged view types
  typedef Kokkos::View<T,L,D,Kokkos::MemoryUnmanaged> t_dev_um;
  typedef Kokkos::View<typename t_host::data_type,typename t_host::array_layout,
                       typename t_host::device_type,Kokkos::MemoryUnmanaged> t_host_um;

  // Define const unmanaged view types
  typedef Kokkos::View<typename t_dev::const_data_type,L,D,Kokkos::MemoryUnmanaged> t_dev_const_um;
  typedef Kokkos::View<typename t_host::const_data_type,typename t_host::array_layout,
                       typename t_host::device_type,Kokkos::MemoryUnmanaged> t_host_const_um;

  /* provide the same typedefs as a view for scalar, data and value types */

  typedef typename t_dev::value_type value_type;
  typedef typename t_dev::const_value_type const_value_type;
  typedef typename t_dev::scalar_type scalar_type;
  typedef typename t_dev::const_scalar_type const_scalar_type;
  typedef typename t_dev::non_const_scalar_type non_const_scalar_type;

  /* Instances of base types */

  t_dev d_view;
  t_host h_view;


  /* Counters to keep track of changes (dirty-flags) */

  unsigned int modified_device;
  unsigned int modified_host;

  /* Return view on specific device via view<Device>() */

  template< class Device >
  const typename Kokkos::Impl::if_c< Kokkos::Impl::is_same< typename t_dev::memory_space ,
                                      typename Device::memory_space >::value ,
                             t_dev , t_host >::type view() const
  {
    return Kokkos::Impl::if_c< Kokkos::Impl::is_same< typename t_dev::memory_space ,
                                typename Device::memory_space >::value ,
                       t_dev , t_host >::select( d_view , h_view );
  }


  /* Construct views */

  /* Empty Constructor */

  DualView() {
    modified_host = 0;
    modified_device = 0;
  }

  /* Create view with allocation on both host and device */

  DualView( const std::string & label ,
    const size_t n0 = 0 ,
    const size_t n1 = 0 ,
    const size_t n2 = 0 ,
    const size_t n3 = 0 ,
    const size_t n4 = 0 ,
    const size_t n5 = 0 ,
    const size_t n6 = 0 ,
    const size_t n7 = 0 )
    : d_view( label, n0, n1, n2, n3, n4, n5, n6, n7 )
    , h_view( create_mirror_view( d_view ) )
  {
    modified_host = 0;
    modified_device = 0;
  }

    void readGPU() {
		SemCacheSet(&h_view(0), &h_view(0) + capacity(), d_view.ptr_on_device(), capacity(), sizeof(h_view(0)) );
		SemCache::readGPU();
	}

	void writeGPU() {
		SemCacheSet(&h_view(0), &h_view(0) + capacity(), d_view.ptr_on_device(), capacity(), sizeof(h_view(0)) );
		SemCache::writeGPU();
	}

  /* Update data on device or host only if other space is polluted */

  template<class Device>
  void sync() {
    unsigned int dev = Kokkos::Impl::if_c< Kokkos::Impl::is_same< typename t_dev::memory_space ,
                                  typename Device::memory_space >::value ,
                                  unsigned int , unsigned int >::select( 1, 0 );

    if(dev) {
      if((modified_host > 0) && (modified_host >= modified_device)) {
      Kokkos::deep_copy(d_view,h_view);
      modified_host = modified_device = 0;
      }
    } else {
      if((modified_device > 0) && (modified_device >= modified_host)) {
      Kokkos::deep_copy(h_view,d_view);
      modified_host = modified_device = 0;
      }
    }
  }

  /* Mark data as dirty on a device */

  template<class Device>
  void modify() {
    unsigned int dev = Kokkos::Impl::if_c< Kokkos::Impl::is_same< typename t_dev::memory_space ,
                                  typename Device::memory_space >::value ,
                                  unsigned int , unsigned int >::select( 1, 0 );

    if(dev) {
      modified_device = (modified_device > modified_host ? modified_device : modified_host)  + 1;
    } else {
      modified_host = (modified_device > modified_host ? modified_device : modified_host)  + 1;
    }
  }

  /* Realloc both views, no deep copy */

  void realloc( const size_t n0 = 0 ,
           const size_t n1 = 0 ,
           const size_t n2 = 0 ,
           const size_t n3 = 0 ,
           const size_t n4 = 0 ,
           const size_t n5 = 0 ,
           const size_t n6 = 0 ,
           const size_t n7 = 0 ) {
     Kokkos::realloc(d_view,n0,n1,n2,n3,n4,n5,n6,n7);
     h_view = create_mirror_view( d_view );

     /* Reset dirty flags */
     modified_device = modified_host = 0;
  }

  /* Resize both views, only do deep_copy in space which was last marked as dirty */

  void resize( const size_t n0 = 0 ,
           const size_t n1 = 0 ,
           const size_t n2 = 0 ,
           const size_t n3 = 0 ,
           const size_t n4 = 0 ,
           const size_t n5 = 0 ,
           const size_t n6 = 0 ,
           const size_t n7 = 0 ) {
   if(modified_device >= modified_host) {
     /* Resize on Device */
     Kokkos::resize(d_view,n0,n1,n2,n3,n4,n5,n6,n7);
     h_view = create_mirror_view( d_view );

     /* Mark Device copy as modified */
     modified_device++;

   } else {
     /* Realloc on Device */

     Kokkos::realloc(d_view,n0,n1,n2,n3,n4,n5,n6,n7);
     t_host temp_view = create_mirror_view( d_view );

     /* Remap on Host */
     Kokkos::Impl::ViewRemap< t_host , t_host >( temp_view , h_view );
     h_view = temp_view;

     /* Mark Host copy as modified */
     modified_host++;
   }
  }

  size_t capacity() const {
    return d_view.capacity();
  }
};
}
#endif
